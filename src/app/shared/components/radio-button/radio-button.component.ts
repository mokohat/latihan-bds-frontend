import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'b-radioButton',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.css']
})
export class RadioButtonComponent implements OnInit {

  @Input() label: string
  @Input() radioName: string
  @Input() disabled: boolean

  @Output() rbClick = new EventEmitter<any>()

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.rbClick.emit(true);
  }

}
