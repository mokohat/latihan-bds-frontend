import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'b-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() label: string;
  @Input() value: any;
  @Input() disabled: boolean;

  @Output() change = new EventEmitter<any>()

  constructor() { }

  ngOnInit() {
  }

  // onChange(evt) {
  //   console.log('check',evt)
  //   this.change.emit(evt.target);
  // }

}
