import { ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'b-dropDown',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.css']
})
export class DropDownComponent implements OnInit {

  @Input() list: any[];
  @Input() spanLabel: string;
  private _disabled: boolean;
  @Input() set disabled(value: boolean) {
    this._disabled = value;
    if(value) {
      document.getElementsByClassName('input-group').item(0).classList.add('disabled');
    } else {
      document.getElementsByClassName('input-group').item(0).classList.remove('disabled');
    }
  }

  get disabled() {
    console.log(this._disabled)
    return this._disabled;
  }

  @Output() focus= new EventEmitter<any>();
  @Output() blur = new EventEmitter<any>();

  @ViewChild("dd",{static:false}) dd: ElementRef;

  arrowDown="<span class='end-add-on'><svg width='48' height='48' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'><path fill-rule='evenodd' clip-rule='evenodd' d='M24 26.4975L12.0191 15.6836C10.9595 14.7272 9.34822 14.7273 8.28871 15.6838C7.0643 16.7892 7.0643 18.7108 8.28871 19.8162L21.3196 31.5802C22.8422 32.9548 25.1578 32.9548 26.6804 31.5802L39.7113 19.8162C40.9357 18.7108 40.9357 16.7892 39.7113 15.6838C38.6518 14.7273 37.0405 14.7272 35.9809 15.6836L24 26.4975Z' fill='#808080'/></svg></span>"

  constructor() {
  }

  ngOnInit() {
  }



  onFocus() {
    this.focus.emit()
    console.log(this.dd.nativeElement.getElementsByClassName('end-add-on'));
    this.dd.nativeElement.getElementsByClassName('end-add-on').item(0).setAttribute('style','transform: rotate(180deg)');
    document.getElementsByClassName("custom-select-options").item(0).classList.remove('hidden-all');
  }

  onBlur() {
    this.blur.emit();
    this.dd.nativeElement.getElementsByClassName('end-add-on').item(0).setAttribute('style','transform: rotate(0deg)');
    document.getElementsByClassName("custom-select-options").item(0).classList.add('hidden-all');
  }

  onClickli(evt) {
    console.log('click>> data',evt);
  }



}
