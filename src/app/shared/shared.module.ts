import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { RadioButtonComponent } from './components/radio-button/radio-button.component';
import { DropDownComponent } from './components/drop-down/drop-down.component';

@NgModule({
  declarations: [CheckboxComponent, RadioButtonComponent, DropDownComponent],
  imports: [
    CommonModule,
  ],
  exports: [
    CheckboxComponent,
    RadioButtonComponent,
    DropDownComponent
  ]
})
export class SharedModule { }
