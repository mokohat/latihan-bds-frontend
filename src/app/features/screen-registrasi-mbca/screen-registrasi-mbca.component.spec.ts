import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenRegistrasiMbcaComponent } from './screen-registrasi-mbca.component';

describe('ScreenRegistrasiMbcaComponent', () => {
  let component: ScreenRegistrasiMbcaComponent;
  let fixture: ComponentFixture<ScreenRegistrasiMbcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenRegistrasiMbcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenRegistrasiMbcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
