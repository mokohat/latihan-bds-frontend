import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCardMbcaComponent } from './product-card-mbca.component';

describe('ProductCardMbcaComponent', () => {
  let component: ProductCardMbcaComponent;
  let fixture: ComponentFixture<ProductCardMbcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCardMbcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardMbcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
