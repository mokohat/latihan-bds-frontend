import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-card-mbca',
  templateUrl: './product-card-mbca.component.html',
  styleUrls: ['./product-card-mbca.component.css']
})
export class ProductCardMbcaComponent implements OnInit {

  private _disableDropDown: boolean = true;
  rbFin: string;
  optionList = [
    {label:"82342627782378",value:"82342627782378"},{label:"823447748489",value:"823447748489"}];

  set disableDropdown(value: boolean) {
    this._disableDropDown = value;
    if(this._disableDropDown) {
      document.getElementsByClassName('dropdown-group').item(0).classList.add('disabled-dropdown');
    } else {
      document.getElementsByClassName('dropdown-group').item(0).classList.remove('disabled-dropdown');
    }
  }

  get disableDropdown() {
    return this._disableDropDown;
  }
  disableRb = true;
  disableBtn = true;
  selectedValue: any;

  item: any;
  constructor() { }

  ngOnInit() {
    if (this.optionList.length === 1) {
      this.disableDropdown = true;
      this.item = this.optionList[0].value;
    } else {
      this.disableDropdown = false;
      this.item = null;
    }
  }


  cbMbcaAction(evt) {
    console.log(evt);
    const checked = evt.target.checked;
    if (this.optionList.length === 1) {
      this.disableDropdown = true;
      this.item = this.optionList[0].value;
    } else {
      this.disableDropdown = false;
      this.item = null;
    }
    if(checked) {
      // this.selectedValue = ;
      this.disableRb = false;
      this.disableBtn = false;
      document.getElementById("product1").setAttribute('style','left: 28px;border: 2px solid #00B5F0;top:12px; border-radius:8px');
    } else {
      this.selectedValue = null;
      this.disableRb = true;
      this.disableBtn = true;
      this.disableDropdown = true;
      document.getElementById("product1").setAttribute('style','left: 28px;border: 2px solid #FFFFFF;top:12px;border-radius:8px');
    }
  }

  selectDropdownData(evt) {
    console.log(evt);
  }

}
