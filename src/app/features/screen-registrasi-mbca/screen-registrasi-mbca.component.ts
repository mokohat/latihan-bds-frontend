import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screen-registrasi-mbca',
  templateUrl: './screen-registrasi-mbca.component.html',
  styleUrls: ['./screen-registrasi-mbca.component.css']
})
export class ScreenRegistrasiMbcaComponent implements OnInit {
  selectedValue: any;

  constructor() { }

  fullName:string;
  cinNumber: string;
  status: string;
  markOptions = {
    text: "12345678",
    fontSize: "10px",
    width: 44,
    height: 44
  }
  labelmBca="m-Bca";
  labelRbFnsl="Finansial"
  labelRbNonFnsl="Non-Finansial"
  rbTipe="tipe1"

  labelKbi="Klik BCA Individu";
  labelNoKeyBca="Tanpa KeyBca";
  labelKeyBca="Dengan KeyBca";

  disableBoxMbca = true;
  disableBoxKbi = true;
  displayModal = false;
  showHeader = false;

  rbValue: string;

  optionList = [
    "65757558",
    "57755858"
  ]


  ngOnInit() {
    this.fullName="Lidya Marselina";
    this.cinNumber="000001234567890 Prioritas  /  K1"
    this.status = "NRT"
  }

  cbMbca(evt) {
    console.log(evt.target.checked);
    const checked = evt.target.checked
    if(checked) {
      this.selectedValue = evt.target.value;
      this.disableBoxMbca = false;
      document.getElementById("product1").setAttribute('style','border: 2px solid #00B5F0;top:12px;');
    } else {
      this.selectedValue = null;
      this.disableBoxMbca = true;
      document.getElementById("product1").setAttribute('style','border: 2px solid #FFFFFF;top:12px;');
    }
  }

  cbKbi(evt) {
    console.log(evt.target.checked);
    const checked = evt.target.checked
    if(checked) {
      this.selectedValue = evt.target.value;
      this.disableBoxKbi = false;
      document.getElementById("product2").setAttribute('style','left: 28px;border: 2px solid #00B5F0;top:12px;');
    } else {
      this.selectedValue = null;
      this.disableBoxKbi = true;
      document.getElementById("product2").setAttribute('style','left: 28px;border: 2px solid #FFFFFF;top:12px;');
    }
  }

  rbClick(evt) {
    console.log(evt.target.value);
  }

  onFocus() {

  }

  onClickInfo() {
    this.displayModal = true
  }

}
