import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-card-kbi',
  templateUrl: './product-card-kbi.component.html',
  styleUrls: ['./product-card-kbi.component.css']
})
export class ProductCardKbiComponent implements OnInit {


  rbKey: any;
  key: string;
  disableRb: boolean = true;

  iconQr = 'icon-qr-off';

  private _disabledInput:boolean = true;
  disableCbBebas = true;

  set disableInput(value: boolean) {
    this._disabledInput = value;
    if(this._disabledInput) {
      this.iconQr = 'icon-qr-off'
      document.getElementById('inputkey').classList.add('input-btn-disabled');
    } else {
      this.iconQr = 'icon-qr-on'
      document.getElementById('inputkey').classList.remove('input-btn-disabled');
    }
  }

  get disableInput() {
    return this._disabledInput;
  }

  constructor() { }

  ngOnInit() {
  }

  cbKbiAction(evt) {
     const checked = evt.target.checked;
     if(checked) {
      // this.selectedValue = ;
      this.disableRb = false;
      this.disableInput = false;
      document.getElementById("product2").setAttribute('style','left: 28px;border: 2px solid #00B5F0;top:12px; border-radius:8px');
    } else {
      this.disableRb = true;
      this.disableInput = true;
      document.getElementById("product2").setAttribute('style','left: 28px;border: 2px solid #FFFFFF;top:12px;border-radius:8px');
    }
  }

  cbBebasAction(evt) {

  }

}
