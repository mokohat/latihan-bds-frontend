import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCardKbiComponent } from './product-card-kbi.component';

describe('ProductCardKbiComponent', () => {
  let component: ProductCardKbiComponent;
  let fixture: ComponentFixture<ProductCardKbiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCardKbiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardKbiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
