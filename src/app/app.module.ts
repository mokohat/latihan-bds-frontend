import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScreenRegistrasiMbcaComponent } from './features/screen-registrasi-mbca/screen-registrasi-mbca.component';
import { SharedModule } from './shared/shared.module';
import { ToolbarModule } from 'primeng-lts/toolbar';
import {CardModule} from 'primeng/card';
import { NgxWatermarkModule } from 'ngx-watermark';
import {CheckboxModule} from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import {ButtonModule} from 'primeng/button';
import { ProductCardMbcaComponent } from './features/screen-registrasi-mbca/product-card-mbca/product-card-mbca.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ProductCardKbiComponent } from './features/screen-registrasi-mbca/product-card-kbi/product-card-kbi.component';
import {DialogModule} from 'primeng/dialog';
@NgModule({
  declarations: [
    AppComponent,
    ScreenRegistrasiMbcaComponent,
    ProductCardMbcaComponent,
    ProductCardKbiComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    ToolbarModule,
    CardModule,
    ButtonModule,
    NgxWatermarkModule,
    CheckboxModule,
    RadioButtonModule,
    FormsModule,
    InputTextModule,
    DropdownModule,
    DialogModule
  ],
  exports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ToolbarModule,
    CardModule,
    ButtonModule,
    NgxWatermarkModule,
    CheckboxModule,
    RadioButtonModule,
    InputTextModule,
    DropdownModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
